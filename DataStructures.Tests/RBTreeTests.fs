﻿namespace DataStructures.Tests

open System
open DataStructures
open NUnit.Framework

[<TestFixture>]
type RBTreeTests() = 
    [<Test>]
    member x.AddingInAscendingOrderAndIterating() =
        let bt = [1 .. 7] |> Seq.fold (fun t x -> RBTree.Insert x (Some (x * x)) t) RBTree.empty
        let expected = [1 .. 7] |> List.map (fun x -> Some (x * x))

        CollectionAssert.AreEqual(expected, RBTree.InorderSeq bt)
        
    [<Test>]
    member x.AddingInDescendingOrderAndIterating() =
        let bt = [7 .. -1 .. 1] |> Seq.fold (fun t x -> RBTree.Insert x (Some (x * x)) t) RBTree.empty
        let expected = [1 .. 7] |> List.map (fun x -> Some (x * x))

        CollectionAssert.AreEqual(expected, RBTree.InorderSeq bt)
        
    [<Test>]
    member x.AddingInRandomOrderAndIterating() =
        let bt = [2; 1; 5; 7; 3; 6 ; 4] |> Seq.fold (fun t x -> RBTree.Insert x (Some (x * x)) t) RBTree.empty
        let expected = [1 .. 7] |> List.map (fun x -> Some (x * x))

        CollectionAssert.AreEqual(expected, RBTree.InorderSeq bt)
        
    [<Test>]
    member x.AddingInRandomOrderToGetRedRootAndIterating() =
        let bt = [6; 1; 7; 3; 5; 2 ; 4] |> Seq.fold (fun t x -> RBTree.Insert x (Some (x * x)) t) RBTree.empty
        let expected = [1 .. 7] |> List.map (fun x -> Some (x * x))

        CollectionAssert.AreEqual(expected, RBTree.InorderSeq bt)