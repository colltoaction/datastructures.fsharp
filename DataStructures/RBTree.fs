﻿namespace DataStructures

type Color = Red | Black
type BaseRBTree<'a, 'b> =
    | EmptyTree
    | TreeNode of Color * BaseRBTree<'a, 'b> * BaseRBTree<'a, 'b> * 'a * 'b

module RBTree =
    let IsEmpty = function
        | EmptyTree -> true
        | _ -> false

    let rec Count = function
        | EmptyTree -> 0
        | TreeNode(_, left, right, _, _) -> 1 + Count left + Count right

    (*  I know that if I'm Red, I'm already balanced or a leaf
        I don't mind returning a red root because:
            a. when going up the recursion it's going to be fixed
            b. if it's the root, then it's handled in Insert *)
    let Balance = function
        | Black, TreeNode(Red, TreeNode(Red, llt, lrt, lk, le), rlt, key, el), rrt, rk, re
        | Black, TreeNode(Red, llt, TreeNode(Red, lrt, rlt, key, el), lk, le), rrt, rk, re
        | Black, llt, TreeNode(Red, TreeNode(Red, lrt, rlt, key, el), rrt, rk, re), lk, le
        | Black, llt, TreeNode(Red, lrt, TreeNode(Red, rlt, rrt, rk, re), key, el), lk, le ->
            TreeNode(Red, TreeNode(Black, llt, lrt, lk, le), TreeNode(Black, rlt, rrt, rk, re), key, el)
        | color, left, right, key, el ->
            TreeNode(color, left, right, key, el)

    let Insert k x tree =
        let rec InnerInsert = function
            | EmptyTree ->
                TreeNode(Black, EmptyTree, EmptyTree, k, x)
            | TreeNode(color, left, right, key, el) ->
                if k = key then TreeNode(color, left, right, k, x)
                elif k < key then Balance (color, InnerInsert left, right, key, el)
                else Balance (color, left, InnerInsert right, key, el)

        (* red root special case *)
        match InnerInsert tree with
        | TreeNode(Red, left, right, key, el) -> TreeNode(Black, left, right, key, el)
        | root -> root

    let rec Find k = function
        | EmptyTree -> None
        | TreeNode(_, left, right, key, el) -> 
            if k = key then el
            elif k < key then Find k left
            else Find k right

    let rec InorderSeq = function
        | EmptyTree -> Seq.empty
        | TreeNode(_, left, right, _, el) -> seq { yield! InorderSeq(left); yield el; yield! InorderSeq(right) }

    let empty = EmptyTree