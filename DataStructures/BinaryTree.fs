﻿namespace DataStructures

type BaseBinaryTree<'a, 'b> =
    | EmptyTree
    | TreeNode of BaseBinaryTree<'a, 'b> * BaseBinaryTree<'a, 'b> * 'a * 'b

module BinaryTree =
    let IsEmpty = function
        | EmptyTree -> true
        | _ -> false

    let rec Count = function
        | EmptyTree -> 0
        | TreeNode(left, right, key, el) -> 1 + Count left + Count right

    let rec Append k x = function
        | EmptyTree -> TreeNode(EmptyTree, EmptyTree, k, x)
        | TreeNode(left, right, key, el) -> 
            if k = key then TreeNode(left, right, k, x)
            elif k < key then TreeNode(Append k x left, right, key, el)
            else TreeNode(left, Append k x right, key, el)

    let rec Rightmost = function
        | EmptyTree -> EmptyTree
        | TreeNode(left, EmptyTree, key, el) as node -> node
        | TreeNode(left, right, key, el) -> Rightmost right

    let rec Delete k = function
        | EmptyTree -> EmptyTree
        | TreeNode(left, right, key, el) as node -> 
            if k = key then RemoveRoot node
            elif k < key then TreeNode(Delete k left, right, key, el)
            else TreeNode(left, Delete k right, key, el)

    and RemoveRoot = function
        | EmptyTree -> EmptyTree
        | TreeNode(EmptyTree, newRoot, _, _) | TreeNode(newRoot, EmptyTree, _, _) -> newRoot
        | TreeNode(left, right, _, _) ->
            let predecesor = Rightmost left
            match predecesor with
            | EmptyTree -> failwith "The predecessor can't be an empty tree"
            | TreeNode(_, _, key, el) -> TreeNode(Delete key left, right, key, el)

    let rec Find k = function
        | EmptyTree -> None
        | TreeNode(left, right, key, el) -> 
            if k = key then el
            elif k < key then Find k left
            else Find k right

    let rec InorderSeq = function
        | EmptyTree -> Seq.empty
        | TreeNode(left, right, key, el) -> seq { yield! InorderSeq(left); yield el; yield! InorderSeq(right) }

    let empty = EmptyTree