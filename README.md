[![Build status](https://ci.appveyor.com/api/projects/status/89i6h4a4eawwpyh4/branch/master?svg=true)](https://ci.appveyor.com/project/tinchou/datastructures-fsharp/branch/master)

DataStructures in F#
---------------------------

As an exercise for my self improvement, I'm implementing various data structures in F#.
