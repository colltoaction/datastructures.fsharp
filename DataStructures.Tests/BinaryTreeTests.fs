﻿namespace DataStructures.Tests

open System
open DataStructures
open NUnit.Framework

[<TestFixture>]
type BinaryTreeTests() = 
    [<Test>]
    member x.AddingInAscendingOrderAndIterating() =
        let bt = [1 .. 7] |> Seq.fold (fun t x -> BinaryTree.Append x (Some (x * x)) t) BinaryTree.empty
        let expected = [1 .. 7] |> List.map (fun x -> Some (x * x))

        CollectionAssert.AreEqual(expected, BinaryTree.InorderSeq bt)
        
    [<Test>]
    member x.AddingInDescendingOrderAndIterating() =
        let bt = [7 .. -1 .. 1] |> Seq.fold (fun t x -> BinaryTree.Append x (Some (x * x)) t) BinaryTree.empty
        let expected = [1 .. 7] |> List.map (fun x -> Some (x * x))

        CollectionAssert.AreEqual(expected, BinaryTree.InorderSeq bt)
        
    [<Test>]
    member x.AddingInRandomOrderAndIterating() =
        let bt = [2; 1; 5; 7; 3; 6 ; 4] |> Seq.fold (fun t x -> BinaryTree.Append x (Some (x * x)) t) BinaryTree.empty
        let expected = [1 .. 7] |> List.map (fun x -> Some (x * x))

        CollectionAssert.AreEqual(expected, BinaryTree.InorderSeq bt)
        
    [<Test>]
    member x.IteratingAfterDeletingNonExistentItem() =
        let bt =
            [2; 1; 5; 7; 3; 6]
            |> Seq.fold (fun t x -> BinaryTree.Append x (Some (x * x)) t) BinaryTree.empty
            |> BinaryTree.Delete 4
        let expected = ([1 .. 3] @ [5 .. 7]) |> List.map (fun x -> Some (x * x))

        CollectionAssert.AreEqual(expected, BinaryTree.InorderSeq bt)
        
    [<Test>]
    member x.IteratingAfterDeletingLeafItem() =
        let bt =
            [2; 1; 5; 7; 3; 6; 4]
            |> Seq.fold (fun t x -> BinaryTree.Append x (Some (x * x)) t) BinaryTree.empty
            |> BinaryTree.Delete 1
        let expected = [2 .. 7] |> List.map (fun x -> Some (x * x))

        CollectionAssert.AreEqual(expected, BinaryTree.InorderSeq bt)
        
    [<Test>]
    member x.IteratingAfterDeletingExistentItemWithOneLeftChild() =
        let bt =
            [2; 1; 5; 7; 3; 6; 4]
            |> Seq.fold (fun t x -> BinaryTree.Append x (Some (x * x)) t) BinaryTree.empty
            |> BinaryTree.Delete 7
        let expected = [1 .. 6] |> List.map (fun x -> Some (x * x))

        CollectionAssert.AreEqual(expected, BinaryTree.InorderSeq bt)
        
    [<Test>]
    member x.IteratingAfterDeletingItemWithOneRightChild() =
        let bt =
            [2; 1; 5; 7; 3; 6; 4]
            |> Seq.fold (fun t x -> BinaryTree.Append x (Some (x * x)) t) BinaryTree.empty
            |> BinaryTree.Delete 3
        let expected = ([1; 2] @ [4 .. 7]) |> List.map (fun x -> Some (x * x))

        CollectionAssert.AreEqual(expected, BinaryTree.InorderSeq bt)
        
    [<Test>]
    member x.IteratingAfterDeletingItemWithTwoChildren() =
        let bt =
            [2; 1; 5; 7; 3; 6; 4]
            |> Seq.fold (fun t x -> BinaryTree.Append x (Some (x * x)) t) BinaryTree.empty
            |> BinaryTree.Delete 5
        let expected = ([1 .. 4] @ [6; 7]) |> List.map (fun x -> Some (x * x))

        CollectionAssert.AreEqual(expected, BinaryTree.InorderSeq bt)
        
    [<Test>]
    member x.IteratingAfterDeletingRootItem() =
        let bt =
            [2; 1; 5; 7; 3; 6; 4]
            |> Seq.fold (fun t x -> BinaryTree.Append x (Some (x * x)) t) BinaryTree.empty
            |> BinaryTree.Delete 2
        let expected = 1 :: [3 .. 7] |> List.map (fun x -> Some (x * x))

        CollectionAssert.AreEqual(expected, BinaryTree.InorderSeq bt)